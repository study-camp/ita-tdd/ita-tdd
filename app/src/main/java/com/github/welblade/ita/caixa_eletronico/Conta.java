package com.github.welblade.ita.caixa_eletronico;

public interface Conta {
    public boolean logar(String numeroConta);
    public boolean sacar(double valor);
    public boolean depositar(double valor);
    public Object getServico();
    public Object getNumero();
    public Double getSaldo();
}
