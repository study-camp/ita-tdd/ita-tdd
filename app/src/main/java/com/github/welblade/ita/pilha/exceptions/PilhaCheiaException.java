package com.github.welblade.ita.pilha.exceptions;


public class PilhaCheiaException extends RuntimeException{
    public PilhaCheiaException(String msg){
        super(msg);
    }
}
