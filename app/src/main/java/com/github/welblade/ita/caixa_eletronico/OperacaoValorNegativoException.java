package com.github.welblade.ita.caixa_eletronico;

public class OperacaoValorNegativoException extends RuntimeException{
    public OperacaoValorNegativoException(String mensagem){
        super(mensagem);
    }
}
