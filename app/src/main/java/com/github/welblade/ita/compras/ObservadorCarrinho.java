package com.github.welblade.ita.compras;

public interface ObservadorCarrinho {
    public void produtoAdicionado(String nome, int valor);
    public void verificarRecebimentoProduto(String nome, int valor);
}
