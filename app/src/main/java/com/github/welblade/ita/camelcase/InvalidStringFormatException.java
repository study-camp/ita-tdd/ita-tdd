package com.github.welblade.ita.camelcase;


public class InvalidStringFormatException extends RuntimeException{
    public InvalidStringFormatException(String message){
        super(message);
    }
}
