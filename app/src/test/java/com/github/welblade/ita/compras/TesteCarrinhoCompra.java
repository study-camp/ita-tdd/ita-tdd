package com.github.welblade.ita.compras;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.github.welblade.ita.compras.helper.MockObservadorCarrinho;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class TesteCarrinhoCompra {
    @Test
    public void totalCarrinho(){
        CarrinhoCompras c = new CarrinhoCompras();
        c.adicionarProduto(new Produto("tenis", 100));
        c.adicionarProduto(new Produto("camiseta", 50));
        c.adicionarProduto(new Produto("bermuda", 70));
        assertEquals(220, c.total());
    }

    @Test
    public void escutaAdicaoProduto(){
        CarrinhoCompras c =  new CarrinhoCompras();
        MockObservadorCarrinho mock = new MockObservadorCarrinho();
        c.adicionarObservador(mock);
        c.adicionarProduto(new Produto("tenis", 100));
        mock.verificarRecebimentoProduto("tenis", 100);
    }

    @Test
    public void adicionaDoisObservadores(){
        CarrinhoCompras c =  new CarrinhoCompras();
        MockObservadorCarrinho mock1 = new MockObservadorCarrinho();
        MockObservadorCarrinho mock2 = new MockObservadorCarrinho();
        c.adicionarObservador(mock1);
        c.adicionarObservador(mock2);
        c.adicionarProduto(new Produto("tenis", 100));
        mock1.verificarRecebimentoProduto("tenis", 100);
        mock2.verificarRecebimentoProduto("tenis", 100);
    }

    @Test
    public void continuarNotificandoAposErroObservador(){
        CarrinhoCompras c =  new CarrinhoCompras();
        MockObservadorCarrinho mock1 = new MockObservadorCarrinho();
        MockObservadorCarrinho mock2 = new MockObservadorCarrinho();
        MockObservadorCarrinho mock3 = new MockObservadorCarrinho();
        c.adicionarObservador(mock1);
        c.adicionarObservador(mock2);
        mock2.forcarErro();
        c.adicionarObservador(mock3);
        c.adicionarProduto(new Produto("tenis", 100));
        mock1.verificarRecebimentoProduto("tenis", 100);
        mock3.verificarRecebimentoProduto("tenis", 100);
    }

}
