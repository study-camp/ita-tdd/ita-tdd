package com.github.welblade.ita.caixa_eletronico.helper;

public class HardwareMockContaInexistente extends HardwareMock {
    @Override
    public String pegarNumeroDaContaCartao() {
        chamadasMetodos.add("pegarNumeroDaContaCartao");
        return "999999-99";
    }

}
