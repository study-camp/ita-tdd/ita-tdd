# TDD – Desenvolvimento de Software Guiado por Testes
## por Instituto Tecnológico da Aeronáutica
### Aulas de Desenvolvimento de Software Guiado por Testes
#### Hands on 

Semana 1 - Conceitos básicos de TDD
- Pilha
- Camel Case

Semana 2 - Contato prático com o TDD, e ciclo de Refatoração.
- Tradutor
- SAB – Sistema de Automação de Biblioteca (Refatoração)

Semana 3 - Contato prático com casos de teste e com dependências entre classes no contexto do TDD
- Carrinho de Compras
- Software de Caixa Eletrônico

Semana 4 - Aprofundamento teórico e prático em técnicas de refatoração.
- Video Locadora (Refatoração)
- Componente de Gamificação
